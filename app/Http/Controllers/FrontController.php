<?php

namespace App\Http\Controllers;

use App\Attraction;
use App\Rental;
use Illuminate\Http\Request;

class FrontController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $rentals = Rental::all();
        $rentals = $rentals->load('pictures');
        return view('index', compact('rentals'));
    }


    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function single(Rental $rental)
    {
        $rental->load('pictures');
        return view('single', compact('rental'));
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function region()
    {
        $attractions = Attraction::all();
        return view('region', compact('attractions'));
    }
}
