<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic;

class Picture extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'filename', 'alt', 'caption', 'rental_id'
    ];

    public static function boot()
    {
        parent::boot();
        static::deleted(function ($instance) {
            if ($instance->filename != null && $instance->filename != ""){
                unlink(public_path('uploads') . $instance->filename);
            }
            return true;
        });
    }

    public function rental() {
        return $this->belongsTo('App\Rental');
    }

    public static function uploadImage(UploadedFile $file, Rental $owner): string
    {
        if (!File::exists(public_path('uploads/rentals/' . $owner->id))):
            File::makeDirectory(public_path('uploads/rentals/' . $owner->id), 0755,true,true);
        endif;
        $name = time() . "." . $file->getClientOriginalExtension();
        $path = "/rentals/{$owner->id}/$name";
        ImageManagerStatic::make($file)->resize("1600", "900")->save(public_path('uploads') . $path);
        return $path;
    }

}
