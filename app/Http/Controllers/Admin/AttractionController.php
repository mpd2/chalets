<?php

namespace App\Http\Controllers\Admin;

use App\Attraction;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AttractionController extends Controller
{
    public function create()
    {
        $attraction = new Attraction();
        $categories = Category::select('id', 'name')->orderBy('name')->get();
        return view('admin.attractions.form', compact('attraction', 'categories'));
    }

    public function store(Request $request)
    {
        $datas = $request->only(['name', 'description', 'category_id']);
        $validator = Validator::make($datas, [
            'name' => ['required', 'string', 'max:255', "unique:attractions,name"],
            'description' => ['nullable', 'string']
        ]);
        if ($validator->fails()):
            return redirect(route('admin.attractions.create'))->withInput($datas)->withErrors($validator->errors());
        endif;
        if ($request->has('filename')):
            $file = $request->only('filename')['filename'];
            if (is_object($file)) :
                $datas['filename'] = Attraction::uploadImage($file);
            endif;
        endif;
        $attraction = Attraction::create($datas);
        $isRecommanded = $request->has('is_recommanded');
        $attraction->update(['is_recommanded' => $isRecommanded]);
        $message = "L'attraction $attraction->name a été créé.";
        return redirect(route('admin.attractions'))->with('success', $message);
    }

    public function edit(Attraction $attraction)
    {
        $categories = Category::select('id', 'name')->orderBy('name')->get();
        return view('admin.attractions.form', compact('attraction', 'categories'));
    }

    public function update(Request $request, Attraction $attraction)
    {
        $datas = $request->only(['name', 'description', 'category_id']);
        $validator = Validator::make($datas, [
            'name' => ['required', 'string', 'max:255', "unique:attractions,name,{$attraction->id}"],
            'description' => ['nullable', 'string']
        ]);
        if ($validator->fails()):
            return redirect(route('admin.attractions.create'))->withInput($datas)->withErrors($validator->errors());
        endif;
        if ($request->has('filename')):
            $file = $request->only('filename')['filename'];
            if (is_object($file)) :
                if($attraction->filename != null && $attraction->filename != ""):
                    unlink(public_path('uploads') . $attraction->filename);
                endif;
                $datas['filename'] = Attraction::uploadImage($file);
            endif;
        endif;
        $datas['is_recommanded'] = $request->has('is_recommanded');
        $attraction->update($datas);
        $message = "L'attraction $attraction->name a été modifiée.";
        return redirect(route('admin.attractions'))->with('success', $message);
    }

    public function delete(Attraction $attraction)
    {
        $message = "L'attraction $attraction->name a été supprimée.";
        $attraction->delete();
        return redirect(route('admin.attractions'))->with('success', $message);
    }
}
