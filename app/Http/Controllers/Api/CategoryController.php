<?php


namespace App\Http\Controllers\Api;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::select('id', 'name')->orderBy('name')->get();
        return response()->json([
            "categories" => $categories
        ]);
    }

    public function store(Request $request)
    {
        $datas = $request->only(['name']);
        $category = Category::create($datas);
        $dto = Category::select('id', 'name')->find($category->id);
        return response()->json($dto);
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $message = $category->name . ' a été supprimé.';
        $category->delete();
        return response()->json([
            'success' => $message
        ]);
    }
}
