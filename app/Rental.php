<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Rental extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','overview','info_rental','price_min','price_max','info_payment','nb_sleeps','nb_bedroom','info_bedroom',
        'nb_bathroom','info_bathroom'
    ];

    public static function boot()
    {
        parent::boot();
        static::deleted(function ($instance) {
            File::deleteDirectory(public_path('uploads/rentals/' . $instance->id));
            return true;
        });
    }

    public function pictures() {
        return $this->hasMany('App\Picture');
    }

    public function services() {
        return  $this->belongsToMany('App\Service', 'rental_service', 'rental_id', 'service_id');
    }
}
