<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // 1. create users
        DB::table('users')->insert([
            'name' => "Kezakoo",
            'email' => "kezakoo.dev@gmail.com",
            'role' => 'developer',
            'password' => '$2y$10$cqvwZQdp0kpGKg9hMor7HuZnmuIZyMnGZyj1G021T5CAHFU6vomoO',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        User::find(1)->markEmailAsVerified();
        DB::table('users')->insert([
            'name' => "Occitan",
            'email' => "thibault.courrieu@outlook.fr",
            'role' => 'developer',
            'password' => bcrypt('albertA11'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        User::find(2)->markEmailAsVerified();
        // 2. create service
        $services = array(
            'Wifi gratuit',
            'Cuisine équipée',
            'TV',
            'Lave-linge',
            'Salon de jardin',
            'Terrasse',
            'WC Séparé'
        );
        foreach ($services as $service):
            DB::table('services')->insert([
                'name' => $service,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        endforeach;
        // 3. create rental
        $rentals = array(
            [
                'name' => 'Couïrou',
                'price_min' => 250,
                'price_max' => 290,
                'nb_sleeps' => 2,
                'info_bedroom' => '1 lit double'
            ],[
                'name' => 'Capio',
                'price_min' => 300,
                'price_max' => 365,
                'nb_sleeps' => 4,
                'info_bedroom' => '1 lit double, banquette 2 couchages'
            ],
        );
        $overview = "Au pied des premières montagnes Pyrénéennes, Quillan, Station Verte, offre une palette d’activités touristiques, sportives et culturelles pour tous, petits et grands, sportifs ou pas. Situé sur un vaste terrain arboré, idéalement situé dans un endroit calme, à seulement 600 mètres des premiers commerces (boulangerie, pharmacie, boucherie, etc.) et le coeur de ville est accessible à pied sans difficultés.";
        $infoRental = "Départ avant 11h00, arrivée à partir de 16h30.";
        $infoPayment = "Chèques vacances acceptés.";
        $infoBathroom = "1 douche, wc";
        foreach ($rentals as $rental):
            DB::table('rentals')->insert([
                'name' => $rental['name'],
                'overview' => $overview,
                'info_rental' => $infoRental,
                'price_min' => $rental['price_min'],
                'price_max' => $rental['price_max'],
                'info_payment' => $infoPayment,
                'nb_sleeps' => $rental['nb_sleeps'],
                'nb_bedroom' => 1,
                'info_bedroom' => $rental['info_bedroom'],
                'nb_bathroom' => 1,
                'info_bathroom' => $infoBathroom,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        endforeach;
        // 4. create pictures
        $pictures = array(
            [
                'filename' => '/rentals/2/1629480183.jpg',
                'alt' => 'Chalet Capio',
                'caption' => 'Chalet Capio',
                'rental_id' => 2
            ],[
                'filename' => '/rentals/2/1629480290.jpg',
                'alt' => 'Le jardin',
                'caption' => 'Le jardin',
                'rental_id' => 2
            ],
        );
        foreach ($pictures as $picture):
            DB::table('pictures')->insert([
                'filename' => $picture['filename'],
                'alt' => $picture['alt'],
                'caption' => $picture['caption'],
                'rental_id' => $picture['rental_id'],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        endforeach;
        // 5. create categories
        $categories = array(
            'Site historique',
            'Site culturel & religieux',
            'Monuments & statues',
            'Musées & Galeries d\'art',
            'Fermes & Vignobles',
            'Bar & Restaurants',
            'Point d\'eau & plage',
            'Espace verts & parcs',
            'Parc d\'attraction',
            'Office du tourisme',
            'Activité de plein air',
            'Activité nautique',
            'Visites guidées'
        );
        foreach ($categories as $category):
            DB::table('categories')->insert([
                'name' => $category,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        endforeach;
        // 6. create attractions
        $attractions = array(
            [
                'name' => 'TPCF Le TRAIN ROUGE',
                'description' => "Partez à la découvert du magnifique paysage de notre région",
                'category_id' => 11,
                'filename' => '/attractions/1631436244.jpg',
                'is_recommanded' => true
            ]
        );
        foreach ($attractions as $attraction):
            DB::table('attractions')->insert([
                'name' => $attraction['name'],
                'description' => $attraction['description'],
                'category_id' => $attraction['category_id'],
                'filename' => $attraction['filename'],
                'is_recommanded' => $attraction['is_recommanded'],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        endforeach;
    }
}
