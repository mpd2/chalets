@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row row-cols-1 row-cols-md-2">
            <div class="col col-md-8">
                <div
                    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                    <h1 class="h2">Les attractions</h1>
                    <div class="btn-toolbar mb-2 mb-md-0">
                        <a class="btn btn-primary mr-2"
                           href="{{ route('admin.attractions.create') }}">Ajouter une attraction</a>
                    </div>
                </div>
                @if(sizeof($attractions) > 0)
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nom</th>
                            <th scope="col">Recommandé</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @endif
                        @forelse($attractions as $attraction)
                            <tr>
                                <td>{{ $attraction->id }}</td>
                                <td>{{ $attraction->name }}</td>
                                <td>@if($attraction->is_recommanded) <span class="badge badge-success">Recommandé</span> @endif</td>
                                <td>
                                    <form id="attraction-destroy-{{$attraction->id}}" action="{{ route('admin.attractions.destroy', [$attraction]) }}" method="POST"
                                          class="d-none">
                                        @method("DELETE")
                                        @csrf
                                    </form>
                                    <a href="{{route('admin.attractions.edit', $attraction)}}" class="btn btn-sm btn-primary">Modifier</a>
                                    <button class="btn btn-sm btn-outline-danger"
                                            href="{{ route('admin.attractions.destroy', $attraction) }}" onclick="event.preventDefault();
                                                     document.getElementById('attraction-destroy-{{$attraction->id}}').submit();"><i
                                            data-feather="trash-2"></i></button>
                                </td>
                            </tr>
                        @empty
                            <div class="alert alert-danger">
                                Aucune attraction
                            </div>
                        @endforelse
                        @if(sizeof($attractions) > 0)
                        </tbody>
                    </table>
                @endif
            </div>
            <div class="col col-md-4 mt-3 mt-md-0">
                @include('widgets.admin.categories')
            </div>
        </div>
    </div>
@endsection
