<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rentals', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->longText('overview');
            $table->string('info_rental')->nullable();
            $table->integer('price_min')->default(0);
            $table->integer('price_max')->default(0);
            $table->string('info_payment')->nullable();
            $table->integer('nb_sleeps')->default(1);
            $table->integer('nb_bedroom')->default(0);
            $table->string('info_bedroom')->nullable();
            $table->integer('nb_bathroom')->default(0);
            $table->string('info_bathroom')->nullable();
            $table->timestamps();
        });

        // PIVOTS
        Schema::create('rental_service', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('rental_id')->unsigned()->index();
            $table->bigInteger('service_id')->unsigned()->index();
            $table->foreign('rental_id')->references('id')->on('rentals')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rentals');
        Schema::dropIfExists('rental_service');
    }
}
