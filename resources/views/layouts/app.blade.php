<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white border-bottom">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/') }}">Nos chalets</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('region') }}">Dans la région</a>
                    </li>
                </ul>
            @auth
                <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @if(Auth::user()->role == 'developer' || Auth::user()->role == 'admin')
                                    <a class="dropdown-item" href="{{ route('admin') }}">
                                        Administration
                                    </a>
                                @endif
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                @endauth
            </div>
        </div>
    </nav>

    <main class="pt-4">
        @yield('content')
        <footer class="bg-white border-top">
            <div class="container py-4">
                <div class="row row-cols-1 row-cols-md-3">
                    <div class="col d-none d-md-block">
                        <img src="/assets/logo_chalets_daude.png" alt="Chalets d'Aude" title="Chalets d'Aude"
                             width="160px"/>
                    </div>
                    <div class="card border-0 col mb-4 mb-md-0">
                        <div class="card-body">
                            <h3 class="h6 text-uppercase font-weight-bold d-flex align-items-center">
                                <i data-feather="mail" class="mr-2"></i>
                                Nous contacter
                            </h3>
                            <p>
                                <strong>Tél : </strong>04 30 07 35 62<br/>
                                <strong>Mail : </strong><a
                                    href="mailto:contact@chalets-aude.fr">contact@chalets-aude.fr</a>
                            </p>
                        </div>
                    </div>
                    <div class="card border-0 col">
                        <div class="card-body">
                            <h3 class="h6 text-uppercase font-weight-bold d-flex align-items-center">
                                <i data-feather="map-pin" class="mr-2"></i>
                                Adresse
                            </h3>
                            <p>
                                Les Chalets d'Aude, <br/>
                                29 avenue de Marides <br/>
                                11500 Quillan, France
                            </p>
                            <p class="small text-muted">
                                chalets-aude.fr &copy; 2021<br/>
                                powered by <a href="mailto:kezakoo.dev@google.com">Kezakoo</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </main>
</div>
</body>
</html>
