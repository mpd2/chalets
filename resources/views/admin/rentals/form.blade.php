@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row row-cols-1 row-cols-md-2">
            <div class="col col-md-8">
                <div
                    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                    <h1 class="h2">@if($rental->id == null) Créer @else Modifier @endif un chalet</h1>
                </div>
                <form method="POST">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="d-flex flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                                <i data-feather="file-text" class="mr-3"></i> <strong>Vue d'ensemble</strong>
                            </div>
                            @if($rental->id != null)
                                @method('PUT')
                            @endif
                            @csrf
                            <div class="mb-3">
                                <label for="name">Nom du chalet</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                       name="name"
                                       value="{{ old('name', $rental->name) }}"
                                       required>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="overview">Description</label>
                                <textarea id="overview" class="form-control"
                                          name="overview">{{ old('overview', $rental->overview) }}</textarea>
                            </div>
                            <div class="mb-3">
                                <label for="info_rental">Information complémentaire</label>
                                <input id="info_rental" type="text"
                                       class="form-control @error('info_rental') is-invalid @enderror"
                                       name="info_rental"
                                       placeholder="Départ avant 11 heures, arrivée à partir de 16 h 30..."
                                       value="{{ old('info_rental', $rental->info_rental) }}">
                                @error('info_rental')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <div class="d-flex flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                                    <i class="icon-euro mr-3"></i> <strong>Prix hebdomadaire</strong>
                                </div>
                                <div class="row row-cols-1 row-cols-md-2 mb-3">
                                    <div class="col mb-3 mb-md-0">
                                        <label for="price_min">Basse saison</label>
                                        <div class="input-group">
                                            <input id="price_min" type="number"
                                                   class="form-control @error('price_min') is-invalid @enderror"
                                                   name="price_min"
                                                   min="0"
                                                   value="{{ old('price_min', $rental->price_min) }}"
                                                   required>
                                            <div class="input-group-append">
                                                <span class="input-group-text">€</span>
                                            </div>
                                        </div>
                                        @error('price_min')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col">
                                        <label for="price_max">Haute saison</label>
                                        <div class="input-group">
                                            <input id="price_max" type="number"
                                                   class="form-control @error('price_max') is-invalid @enderror"
                                                   name="price_max"
                                                   min="0"
                                                   value="{{ old('price_max', $rental->price_max) }}"
                                                   required>
                                            <div class="input-group-append">
                                                <span class="input-group-text">€</span>
                                            </div>
                                        </div>
                                        @error('price_max')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <label for="info_payment">Information complémentaire</label>
                                <input id="info_payment" type="text"
                                       class="form-control @error('info_payment') is-invalid @enderror"
                                       name="info_payment"
                                       placeholder="Carte bleu, liquide, chèques vacances acceptés..."
                                       value="{{ old('info_payment', $rental->info_payment) }}">
                                @error('info_payment')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <div class="d-flex flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                                    <i class="icon-users mr-3"></i> <strong>Couchage(s)</strong>
                                </div>
                                <input id="nb_sleeps" type="number"
                                       class="form-control @error('nb_sleeps') is-invalid @enderror"
                                       name="nb_sleeps"
                                       min="1"
                                       max="16"
                                       value="{{ old('nb_sleeps', $rental->nb_sleeps) }}"
                                       required>
                                @error('nb_sleeps')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <div class="d-flex flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                                    <i class="icon-bed mr-3"></i> <strong>Chambre(s)</strong>
                                </div>
                                <div class="row row-cols-1 row-cols-md-2">
                                    <div class="col col-md-4 mb-3 mb-md-0">
                                        <label for="nb_bedroom">Nombre</label>
                                        <input id="nb_bedroom" type="number"
                                               class="form-control @error('nb_bedroom') is-invalid @enderror"
                                               name="nb_bedroom"
                                               min="0"
                                               value="{{ old('nb_bedroom', $rental->nb_bedroom) }}"
                                               required>
                                        @error('nb_bedroom')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col col-md-8">
                                        <label for="info_bedroom">Information complémentaire</label>
                                        <input id="info_bedroom" type="text"
                                               class="form-control @error('info_bedroom') is-invalid @enderror"
                                               name="info_bedroom"
                                               placeholder="1 lit double, 1 lit simple, 1 canapé convertible..."
                                               value="{{ old('info_bedroom', $rental->info_bedroom) }}">
                                        @error('info_bedroom')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="d-flex flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                                    <i class="icon-shower mr-3"></i> <strong>Salle(s) de bain</strong>
                                </div>
                                <div class="row row-cols-1 row-cols-md-2">
                                    <div class="col col-md-4 mb-3 mb-md-0">
                                        <label for="nb_bathroom">Nombre</label>
                                        <input id="nb_bathroom" type="number"
                                               class="form-control @error('nb_bathroom') is-invalid @enderror"
                                               name="nb_bathroom"
                                               min="0"
                                               value="{{ old('nb_bathroom', $rental->nb_bathroom) }}"
                                               required>
                                        @error('nb_bathroom')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col col-md-8">
                                        <label for="info_bathroom">Information complémentaire</label>
                                        <input id="info_bathroom" type="text"
                                               class="form-control @error('info_bathroom') is-invalid @enderror"
                                               name="info_bathroom"
                                               placeholder="1 douche, 1 baignoire..."
                                               value="{{ old('info_bathroom', $rental->info_bathroom) }}">
                                        @error('info_bathroom')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="d-flex flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                                    <i data-feather="plus-circle" class="mr-3"></i> <strong>Services</strong>
                                </div>
                                <select multiple="multiple" class="multiple-selector form-control" name="services[]"
                                        id="services">
                                    @foreach($services as $service)
                                        @if($rental->id != null)
                                            <option value="{{$service->id}}"
                                                    @if(in_array($service->id,$rental->services->getQueueableIds())) selected @endif>
                                                {{$service->name}}
                                            </option>
                                        @else
                                            <option value="{{$service->id}}">{{$service->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="text-right">
                                <button class="btn btn-primary" type="submit"><i data-feather="save" class="mr-2"></i>
                                    Enregitrer
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                @if($rental->id != null)
                    <div
                        class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                        <h1 class="h2">Les photos</h1>
                    </div>
                    @if(sizeof($rental->pictures) > 0)
                        <div class="row row-cols-1 row-cols-md-2 mt-4">
                            @endif
                            @forelse($rental->pictures as $picture)
                                <div class="col">
                                    <div class="card mb-3">
                                        <img src="{{ "/uploads" . $picture->filename }}" alt="{{$picture->alt}}"
                                             class="card-img-top">
                                        <div class="card-body">
                                            <h5 class="card-title">{{ $picture->caption }}</h5>
                                            <form id="picture-destroy-{{$picture->id}}"
                                                  action="{{ route('admin.rentals.pictures.destroy', [$rental,$picture]) }}"
                                                  method="POST"
                                                  class="d-none">
                                                @method("DELETE")
                                                @csrf
                                            </form>
                                            <p class="card-text text-right">
                                                <a href="{{ route('admin.rentals.pictures.destroy', [$rental,$picture]) }}"
                                                   class="btn btn-sm btn-outline-danger"
                                                   onclick="event.preventDefault();
                                                       document.getElementById('picture-destroy-{{$picture->id}}').submit();">Supprimer</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <div class="alert alert-danger">
                                    Aucune photo
                                </div>
                            @endforelse
                            @if(sizeof($rental->pictures) > 0)
                        </div>
                    @endif
                @endif
            </div>
            <div class="col col-md-4 mt-3 mt-md-0">
                @if($rental->id != null)
                    <div
                        class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                        <h1 class="h2">Ajouter une photo</h1>
                    </div>
                    <div class="card mb-3">
                        <div class="card-body">
                            <form method="POST" action="{{ route('admin.rentals.pictures.store',$rental) }}"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="mb-3">
                                    <label for="alt">{{ __('Alt') }}</label>
                                    <input id="alt" type="text" class="form-control @error('alt') is-invalid @enderror"
                                           name="alt"
                                           required>

                                    @error('alt')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="mb-4">
                                    <label for="caption">Légende</label>
                                    <input id="caption" type="text"
                                           class="form-control @error('caption') is-invalid @enderror"
                                           name="caption"
                                           required>

                                    @error('caption')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="mb-4">
                                    <input type="file" id="filename" name="filename">
                                </div>
                                <div class="alert alert-info mb-3">
                                    <strong><u>Recommandation :</u></strong> format 16:9, avec une largeur minimum de
                                    1600
                                    pixels et une hauteur minimum de 900 pixels.
                                </div>
                                <div class="text-right">
                                    <button class="btn btn-primary" type="submit">
                                        <i data-feather="save" class="mr-2"></i>
                                        Enregistrer
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                @endif
            </div>
        </div>
@endsection
