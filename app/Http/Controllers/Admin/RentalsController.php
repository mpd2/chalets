<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Rental;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RentalsController extends Controller
{

    public function create() {
        $rental = new Rental();
        $services = Service::select('id','name')->orderBy('name')->get();
        return view('admin.rentals.form',compact('services','rental'));
    }

    public function store(Request $request) {
        $datas = $request->only(['name','overview','info_rental','price_min','price_max','info_payment','nb_sleeps',
            'nb_bedroom','info_bedroom', 'nb_bathroom','info_bathroom']);
        $validator = Validator::make($datas, [
            'name' => ['required', 'string', 'max:255', "unique:rentals,name"],
            'overview' => ['required', 'string'],
            'info_rental' => ['nullable','string', 'max:255'],
            'price_min' => ['required', 'integer'],
            'price_max' => ['required', 'integer'],
            'info_payment' => ['nullable','string', 'max:255'],
            'nb_sleeps' => ['required', 'integer'],
            'nb_bedroom' => ['required', 'integer'],
            'info_bedroom' => ['nullable','string', 'max:255'],
            'nb_bathroom' => ['required', 'integer'],
            'info_bathroom' => ['nullable','string', 'max:255']
        ]);
        if ($validator->fails()):
            return redirect(route('admin.rentals.create'))->withInput($datas)->withErrors($validator->errors());
        endif;
        $rental = Rental::create($datas);
        if ($request->has('services')):
            $rental->services()->sync($request->get('services'));
        endif;
        $message = "Le chalet $rental->name a été créé.";
        return redirect(route('admin'))->with('success',$message);
    }

    public function edit(Rental $rental) {
        $rental = $rental->load('pictures');
        $services = Service::select('id','name')->orderBy('name')->get();
        return view('admin.rentals.form',compact('services','rental'));
    }

    public function update(Request $request, Rental $rental) {
        $datas = $request->only(['name','overview','info_rental','price_min','price_max','info_payment','nb_sleeps',
            'nb_bedroom','info_bedroom', 'nb_bathroom','info_bathroom']);
        $validator = Validator::make($datas, [
            'name' => ['required', 'string', 'max:255', "unique:rentals,name,{$rental->id}"],
            'overview' => ['required', 'string'],
            'info_rental' => ['nullable','string', 'max:255'],
            'price_min' => ['required', 'integer'],
            'price_max' => ['required', 'integer'],
            'info_payment' => ['nullable','string', 'max:255'],
            'nb_sleeps' => ['required', 'integer'],
            'nb_bedroom' => ['required', 'integer'],
            'info_bedroom' => ['nullable','string', 'max:255'],
            'nb_bathroom' => ['required', 'integer'],
            'info_bathroom' => ['nullable','string', 'max:255']
        ]);
        if ($validator->fails()):
            return redirect(route('admin.rentals.edit'))->withInput($datas)->withErrors($validator->errors());
        endif;
        $rental->update($datas);
        if ($request->has('services')):
            $rental->services()->sync($request->get('services'));
        endif;
        $message = "Le chalet $rental->name a été modifié.";
        return redirect(route('admin'))->with('success',$message);
    }

    public function destroy(Rental $rental) {;
        $message = "Le chalet $rental->name a été supprimé.";
        $rental->delete();
        return redirect(route('admin'))->with('success',$message);
    }
}
