<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

    public function index() {
        $services = Service::select('id','name')->orderBy('name')->get();
        return response()->json([
            'services' => $services
        ]);
    }

    public function store(Request $request) {
        $datas = $request->only(['name']);
        $service = Service::create($datas);
        $dto = Service::select('id','name')->find($service->id);
        return response()->json($dto);
    }

    public function destroy($id) {
        $service = Service::find($id);
        $message = $service->name.' a été supprimé.';
        $service->delete();
        return response()->json([
            'success' => $message
        ]);
    }

}
