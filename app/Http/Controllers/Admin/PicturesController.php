<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Picture;
use App\Rental;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PicturesController extends Controller
{

    public function store(Request $request, Rental $rental) {
        $datas = $request->only(['alt', 'caption','filename']);
        $validator = Validator::make($datas, [
            'alt' => ['required', 'string', 'max:255'],
            'caption' => ['required', 'string', 'max:255'],
            'filename' => ['required', 'image']
        ]);
        if ($validator->fails()):
            return redirect(route('admin.rentals.edit', $rental))->withErrors($validator->errors());
        endif;
        $datas['rental_id'] = $rental->id;

        $file = $request->only('filename')['filename'];
        if (is_object($file)) :
            $datas['filename'] = Picture::uploadImage($file, $rental);
        endif;
        $picture = Picture::create($datas);
        return redirect(route('admin.rentals.edit', $rental))->with('success', "La photo a été ajoutée.");
    }


    public function destroy(Rental $rental, Picture $picture) {
        $picture->delete();
        return redirect(route('admin.rentals.edit', $rental))->with('success', "La photo a été supprimée.");
    }
}
