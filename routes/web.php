<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index');
Route::get('/rentals/{rental}', 'FrontController@single')->name('rentals.single');
Route::get('/region-pyrennees-audoise', 'FrontController@region')->name('region');

Auth::routes([
    'register' => false,
    'verify' => true
]);
Route::middleware(['verified','can_admin'])->group(function () {

    Route::get('/admin', 'Admin\AdminController@index')->name('admin');
    Route::prefix('/admin')->group(function () {
        Route::get('/attractions', 'Admin\AdminController@attractions')->name('admin.attractions');
        Route::get('/attractions/create', 'Admin\AttractionController@create')->name('admin.attractions.create');
        Route::post('/attractions/create', 'Admin\AttractionController@store');
        Route::get('/attractions/{attraction}', 'Admin\AttractionController@edit')->name('admin.attractions.edit');
        Route::put('/attractions/{attraction}', 'Admin\AttractionController@update');
        Route::delete('/attractions/{attraction}', 'Admin\AttractionController@delete')->name('admin.attractions.destroy');


        Route::get('/rentals/create', 'Admin\RentalsController@create')->name('admin.rentals.create');
        Route::post('/rentals/create', 'Admin\RentalsController@store');
        Route::get('/rentals/{rental}', 'Admin\RentalsController@edit')->name('admin.rentals.edit');
        Route::put('/rentals/{rental}', 'Admin\RentalsController@update');
        Route::delete('/rentals/{rental}', 'Admin\RentalsController@destroy')->name('admin.rentals.destroy');
        Route::post('/rentals/{rental}/pictures/create', 'Admin\PicturesController@store')->name('admin.rentals.pictures.store');
        Route::delete('/rentals/{rental}/pictures/{picture}', 'Admin\PicturesController@destroy')->name('admin.rentals.pictures.destroy');
    });
});
