@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row row-cols-1 row-cols-md-2">
            <div class="col col-md-8">
                <div
                    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                    <h1 class="h2">Nos chalets</h1>
                    <div class="btn-toolbar mb-2 mb-md-0">
                        <a class="btn btn-primary mr-2"
                           href="{{route('admin.rentals.create')}}">Ajouter un chalet</a>
                    </div>
                </div>
                @if(sizeof($rentals) > 0)
                    <div class="row row-cols-1 row-cols-md-2">
                @endif
                @forelse ($rentals as $rental)
                    <div class="col">
                        <div class="card mb-4">
                            @if(sizeof($rental->pictures) > 0)
                                <img src="{{ "/uploads" . $rental->pictures[0]->filename }}" alt="{{$rental->pictures[0]->alt}}" class="card-img-top">
                            @else
                                <img src="/assets/chalets_daude_default.png" alt="Chalets d'Aude" class="card-img-top">
                            @endif
                            <div class="card-body">
                                <h2 class="h5">Chalet {{ $rental->name }}</h2>
                                <div class="d-flex align-items-center justify-content-between">
                                    <small><i class="icon-users text-primary mr-2"></i>{{ $rental->nb_sleeps }} couchage(s)</small>
                                    <small><i class="icon-bed text-primary mr-2"></i>{{ $rental->nb_bedroom }} chambre(s)</small>
                                    <small><i class="icon-shower text-primary mr-2"></i>{{ $rental->nb_bathroom }} salle(s) de bain</small>
                                </div>
                                <div class="text-right pt-3">
                                    <form id="rental-destroy-{{$rental->id}}"
                                          action="{{ route('admin.rentals.destroy', [$rental]) }}" method="POST"
                                          class="d-none">
                                        @method("DELETE")
                                        @csrf
                                    </form>
                                    <a href="{{ route('admin.rentals.edit', [$rental]) }}" class="btn btn-sm btn-success">Modifier</a>
                                    <a class="btn btn-sm btn-outline-danger"
                                       href="{{ route('admin.rentals.destroy', [$rental]) }}"
                                       onclick="event.preventDefault();
                                           document.getElementById('rental-destroy-{{$rental->id}}').submit();"><i
                                            data-feather="trash-2"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="alert alert-danger">
                        Aucun chalet
                    </div>
                @endforelse
                @if(sizeof($rentals) > 0)
                    </div>
                @endif
            </div>
            <div class="col col-md-4 mt-3 mt-md-0">
                @include('widgets.admin.services')
            </div>
    </div>
@endsection
