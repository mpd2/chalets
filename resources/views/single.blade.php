@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row row-cols-1 row-cols-md-2 mb-4">
            <div class="col col-md-10">
                <div
                    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                    <h1 class="h2 text-uppercase">Chalet {{$rental->name}}</h1>
                </div>
                <div class="mb-4">
                    @if(sizeof($rental->pictures) > 1)
                        <carousel-component>
                            @foreach($rental->pictures as $picture)
                                <carousel-slide-component>
                                    <img src="{{ "/uploads" . $picture->filename }}" alt="{{$picture->alt}}"
                                         title="{{$picture->caption}}" width="100%"/>
                                    <a href="{{ "/uploads" . $picture->filename }}"
                                       class="btn-lightbox btn-maximize"
                                       data-lightbox="carousel"
                                       data-alt="{{$picture->alt}}"
                                       data-title="{{$picture->caption}}"
                                    ><i data-feather="maximize"></i></a>
                                </carousel-slide-component>
                            @endforeach
                        </carousel-component>
                    @elseif(sizeof($rental->pictures) > 0)
                        <img src="{{ "/uploads" . $rental->pictures[0]->filename }}" alt="{{$rental->pictures[0]->alt}}"
                             title="{{$rental->pictures[0]->caption}}" width="100%"/>
                    @else
                        <img src="/assets/chalets_daude_default.png" alt="Chalets d'Aude" title="Chalets d'Aude"
                             width="100%"/>
                    @endif
                </div>
                <tabs-component>
                    <tab-component name="Infos" :selected="true">
                        <div class="tab-pane p-4 pb-0 border-bottom border-left border-right rounded-bottom bg-white">
                            <div class="row row-cols-2 row-cols-md-4 mb-4 small">
                                <div class="col"><i class="icon-users text-primary mr-2"></i>{{ $rental->nb_sleeps }}
                                    couchage(s)
                                </div>
                                <div class="col"><i class="icon-bed text-primary mr-2"></i>{{ $rental->nb_bedroom }}
                                    chambre(s)
                                </div>
                                <div class="col"><i class="icon-shower text-primary mr-2"></i>{{ $rental->nb_bathroom }}
                                    salle(s) de bain
                                </div>
                                <div class="col"><i data-feather="moon" class="text-primary mr-1"></i> 1 semaine minimum
                                </div>
                            </div>
                            <h3 class="h4 font-weight-bold">Vue d'ensemble</h3>
                            <p class="border-bottom pb-4">{{ $rental->overview }}</p>
                            <h3 class="h5 font-weight-bold">Chambre(s)</h3>
                            <p class="border-bottom pb-4">{{ $rental->info_bedroom }}</p>
                            <h3 class="h5 font-weight-bold">Salle(s) de bain</h3>
                            <p>{{ $rental->info_bathroom }}</p>
                            @if(!empty($rental->info_rental))
                                <div class="alert alert-info mt-4">{{ $rental->info_rental }}</div>
                            @endif
                        </div>
                    </tab-component>
                    <tab-component name="Services">
                        <div class="tab-pane p-4 pb-0 border-bottom border-left border-right rounded-bottom bg-white">
                            @if(sizeof($rental->services) > 0)
                                <div class="row row-cols-2 row-cols-md-4">
                                    @endif
                                    @forelse ($rental->services as $service)
                                        <div class="col mb-2">
                                            <i data-feather="check" class="text-success mr-2"></i> {{ $service->name }}
                                        </div>
                                    @empty
                                        <div class="alert alert-danger">
                                            Aucun service renseigné.
                                        </div>
                                    @endforelse
                                    @if(sizeof($rental->services) > 0)
                                </div>
                            @endif
                        </div>
                    </tab-component>
                    <tab-component name="Réservation">
                        <div class="tab-pane p-4 border-bottom border-left border-right rounded-bottom bg-white">
                            <h3 class="h5 font-weight-bold">Tarification</h3>
                            <strong>Haute saison : </strong> {{$rental->price_max}}€/semaine<br/>
                            <strong>Basse saison : </strong> {{$rental->price_min}}€/semaine<br/>
                            @if(!empty($rental->info_payment))
                                <div class="alert alert-info mt-2">{{ $rental->info_payment }}</div>
                            @endif
                            <h3 class="h5 font-weight-bold">Réserver</h3>
                            Pour réserver, contactez-nous :<br/><br/>
                            <strong>Tél : </strong>04 30 07 35 62<br/>
                            <strong>Mail : </strong><a href="mailto:contact@chalets-aude.fr">contact@chalets-aude.fr</a>
                        </div>
                    </tab-component>
                </tabs-component>
            </div>
            <div class="col col-md-2">
            </div>
        </div>
    </div>
@endsection
