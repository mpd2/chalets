<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic;

class Attraction extends Model
{

    protected $fillable = [
        'name', 'description','address','is_recommanded', 'filename','category_id'
    ];

    public static function boot()
    {
        parent::boot();
        static::deleted(function ($instance) {
            if ($instance->filename != null && $instance->filename != ""){
                unlink(public_path('uploads') . $instance->filename);
            }
            return true;
        });
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public static function uploadImage(UploadedFile $file): string
    {
        if (!File::exists(public_path('uploads/attractions'))):
            File::makeDirectory(public_path('uploads/attractions'), 0755,true,true);
        endif;
        $name = time() . "." . $file->getClientOriginalExtension();
        $path = "/attractions/$name";
        ImageManagerStatic::make($file)->resize("800", "450")->save(public_path('uploads') . $path);
        return $path;
    }
}
