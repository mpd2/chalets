<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function rentals() {
        return  $this->belongsToMany('App\Rental', 'rental_service', 'service_id', 'rental_id');
    }
}
