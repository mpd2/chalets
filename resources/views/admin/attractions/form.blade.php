@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row row-cols-1 row-cols-md-2">
            <div class="col col-md-8">
                <div
                    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                    <h1 class="h2">@if($attraction->id == null) Créer @else Modifier @endif une attraction</h1>
                </div>
                <form method="POST" enctype="multipart/form-data">
                    <div class="card mb-3">
                        <div class="card-body">
                            @if($attraction->id != null)
                                @method('PUT')
                            @endif
                            @csrf
                            <div class="mb-3">
                                <label for="name">Nom de l'attraction</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                       name="name"
                                       value="{{ old('name', $attraction->name) }}"
                                       required>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <div class="mb-4">
                                <label for="description">Description</label>
                                <textarea id="description" class="form-control"
                                          name="description">{{ old('description', $attraction->description) }}</textarea>
                            </div>
                            <div class="mb-3">
                                <label for="category_id">Catégorie</label>
                                <select class="form-control" name="category_id"
                                        id="category_id">
                                    @foreach($categories as $key => $category)
                                        <option value="{{$category['id']}}"
                                                @if($category['id'] == $attraction->category_id) selected @endif>{{$category['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-4">
                                <input type="file" id="filename" name="filename">
                            </div>
                            <div class="alert alert-info mb-3">
                                <strong><u>Recommandation :</u></strong> format 16:9, avec une largeur minimum de
                                800
                                pixels et une hauteur minimum de 450 pixels.
                            </div>
                            <div class="form-group form-check mb-4">
                                <input type="checkbox" class="form-check-input" id="is_recommanded"
                                       name="is_recommanded" @if($attraction->is_recommanded != null) checked @endif>
                                <label class="form-check-label" for="is_recommanded">Nous recommandons cette
                                    attraction</label>
                            </div>
                            <div class="text-right">
                                <button class="btn btn-primary" type="submit"><i data-feather="save" class="mr-2"></i>
                                    Enregitrer
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col col-md-4 mt-3 mt-md-0">

            </div>
        </div>
@endsection
