<?php

namespace App\Http\Controllers\Admin;

use App\Attraction;
use App\Category;
use App\Http\Controllers\Controller;
use App\Rental;

class AdminController extends Controller
{
    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $rentals = Rental::all();
        $rentals = $rentals->load('pictures');
        return view('admin.rentals', compact('rentals'));
    }


    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function attractions()
    {
        $categories = Category::select('id','name')->orderBy('name')->get();
        $attractions = Attraction::all();
        return view('admin.attractions',compact('attractions','categories'));
    }
}
