<?php

use App\Http\Resources\ServiceDTO;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/services', 'Api\ServiceController@index')->name('api.services.index');
Route::prefix('/service')->group(function () {
    Route::post('/store', 'Api\ServiceController@store')->name('api.services.store');
    Route::delete('/{id}', 'Api\ServiceController@destroy')->name('api.services.destroy');
});
Route::get('/categories', 'Api\CategoryController@index')->name('api.categories.index');
Route::prefix('/category')->group(function () {
    Route::post('/store', 'Api\CategoryController@store')->name('api.categories.store');
    Route::delete('/{id}', 'Api\CategoryController@destroy')->name('api.categories.destroy');
});
