@extends('layouts.app')

@section('content')
<div class="container">
    @if(sizeof($rentals) > 0)
        <div class="row row-cols-1 row-cols-md-2">
            @endif
            @forelse ($rentals as $rental)
                <div class="col">
                    <div class="card mb-4">
                        @if(sizeof($rental->pictures) > 0)
                            <img src="{{ "/uploads" . $rental->pictures[0]->filename }}" alt="{{$rental->pictures[0]->alt}}" class="card-img-top">
                        @else
                            <img src="/assets/chalets_daude_default.png" alt="Chalets d'Aude" class="card-img-top">
                        @endif
                        <div class="card-body">
                            <h2 class="h5">Chalet {{ $rental->name }}</h2>
                            <div class="d-flex align-items-center justify-content-between">
                                <small class="p-2 flex-fill "><i class="icon-users text-primary mr-2"></i>{{ $rental->nb_sleeps }} couchage(s)</small>
                                <small class="p-2 flex-fill"><i class="icon-bed text-primary mr-2"></i>{{ $rental->nb_bedroom }} chambre(s)</small>
                                <small class="p-2 flex-fill"><i class="icon-shower text-primary mr-2"></i>{{ $rental->nb_bathroom }} salle(s) de bain</small>
                            </div>
                            <div class="text-center pt-4">
                               <a href="{{route('rentals.single', [$rental])}}" class="btn btn-lg btn-ocre btn-radius px-5 text-uppercase font-weight-bold border">Découvrir</a>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="alert alert-danger">
                    Aucun chalet
                </div>
            @endforelse
            @if(sizeof($rentals) > 0)
        </div>
    @endif
</div>
@endsection
