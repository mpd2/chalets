@extends('layouts.app')

@section('content')
<div class="container">
    @if(sizeof($attractions) > 0)
        <div class="row row-cols-1 row-cols-md-3">
            @endif
            @forelse ($attractions as $attraction)
                <div class="col">
                    <div class="card mb-4">
                        @if($attraction->filename != null)
                            <img src="{{ "/uploads" . $attraction->filename }}" alt="{{$attraction->name}}" class="card-img-top">
                        @else
                            <img src="/assets/chalets_daude_default.png" alt="{{$attraction->name}}" class="card-img-top">
                        @endif
                        <div class="card-body">
                            <h2 class="h5 d-flex justify-content-between align-items-center">{{ $attraction->name }} @if($attraction->is_recommanded)<i data-feather="star"></i>@endif</h2>
                            <p class="small text-muted">{{ $attraction->category->name }}</p>
                            <p>{{ $attraction->description }}</p>

                        </div>
                    </div>
                </div>
            @empty
                <div class="alert alert-danger">
                    Aucune attraction
                </div>
            @endforelse
            @if(sizeof($attractions) > 0)
        </div>
    @endif
</div>
@endsection
