
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('admin-service-component', require('./components/AdminServiceComponent.vue').default);
Vue.component('carousel-component', require('./components/carousel/CarouselComponent.vue').default);
Vue.component('carousel-slide-component', require('./components/carousel/CarouselSlideComponent.vue').default);
Vue.component('tabs-component', require('./components/tabs/TabsComponent.vue').default);
Vue.component('tab-component', require('./components/tabs/TabComponent.vue').default);
Vue.component('admin-category-component', require('./components/AdminCategoryComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

/**
 * https://feathericons.com/
 */
window.feather = require('feather-icons');
window.feather.replace();

/**
 * select2
 */
require('select2');
$('select[multiple]').select2({
    width: "100%"
});

/**
 * Lightbox2 : https://lokeshdhakar.com/projects/lightbox2
 */
require('lightbox2');
